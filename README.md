# Thanos

Thanos is HA and long term storage solution for Prometheus:
https://thanos.io/

This role will provision Thanos and setup following Thanos components:
  - Sidecar
  - Store
  - Query
  - Compact

As for architecture, please, consult Thanos documentation how and where
components should be provisioned.

This role is tested on Debian 11, but will most probably work on any systemd
Linux system without changes.

This Role is universal for all Thanos server and flag for required components
should be set to True in order to procision them.
By default all flags are set to false and only Thanos binary will be installed.

The idea is to have one simple role which can be called for all Thanos servers
provisioning right components.

Only basic parameters for components are added. For full list, please consult:

  - https://github.com/thanos-io/thanos/blob/main/docs/components/sidecar.md
  - https://github.com/thanos-io/thanos/blob/main/docs/components/store.md
  - https://github.com/thanos-io/thanos/blob/main/docs/components/query.md
  - https://github.com/thanos-io/thanos/blob/main/docs/components/compact.md

Config parameters are to be addedd in templates/etc/thanos/*.conf.j2 for
specific component.

## Object Storate bucket

Thanos requires one object storage bucket to operate. Role is tested against
AWS s3 bucket.

Folowing varaibles are to be defined:

    thanos_bucket_name

Bucket name. Example: "mythanosbucket"

    thanos_bucket_endpoint

Bucket endpoint. Example: "s3.amazonaws.com"

    thanos_bucket_region

Bucket region. Example:  "us-east-1"

    thanos_bucket_access_key
    thanos_bucket_secret_key

Bucket access keypair. Make sure this information is vaulted in Ansible to prevent abuse!

## Role defaults

    thanos_shell_user
    thanos_shell_group
    thanos_homedir

Thanos operates as Prometheus user. In that sense, it is recommended to use same username
specs across Prometheus/Thanos servers.

    thanos_version

Thanos version.

    thanos_architecture

Thanos architecture

    thanos_install_dir

Thanos installation directory where Thanos binary will be stored.

    thanos_config_dir

Thanos config directory. All relevant Thanos daemons config files will be stored here.

    thanos_tsdb_path

Directory where Prometheus tsdb is located.

    thanos_prometheus_url

How to reach Prometheus web interface. Important for Sidecar.

    thanos_bucket_conf

Location of object storate bucket file. Important to keep safe aince it contains sensitive
information.

    thanos_bucket_name
    thanos_bucket_endpoint
    thanos_bucket_region
    thanos_bucket_access_key
    thanos_bucket_secret_key

Parameters for object storage bucket (desscribed in previous chapter).
Do NOT forget to vault keypar info!


    flg_thanos_compact
    flg_thanos_query
    flg_thanos_sidecar
    flg_thanos_store

By default, no Thanos components will be configured. This role is universal for all Thanos related
servers and flag should be set to True where we want specific component to be provisioned.

    thanos_compact_datadir

Thanos Compact data directory. Compact requires usually at least 100GB of space.

    thanos_store_datadir

Thanos Store data directory. Very small ammount of data will be written there.
Thanos compoenents gRPC and HTTP ports (where applicable). Default ports will work if we run only
one Thanos component per server. However, if we run several on the same server, we need to make
sure ports do not clash.

    thanos_compact_http_port
    thanos_query_grpc_port
    thanos_query_http_port
    thanos_sidecar_grpc_port
    thanos_sidecar_http_port
    thanos_store_grpc_port
    thanos_store_http_port

Thanos list of sidecar instances (on prometheus nodes)

Example:

```
thanos_sidecars:
  - prometheus1.mydomain.net
  - prometheus2.mydomain.net
```

    thanos_sidecars: {}
